package ua.nure.yaritenko.Practice2;

import java.util.Iterator;

public class MyListImpl implements MyList, ListIterable {
    Object[] mas;

    MyListImpl() {
        mas = new Object[0];
    }

    public Iterator<Object> iterator() {
        return new IteratorImpl();
    }

    private class IteratorImpl implements Iterator<Object> {
        int nextElement = 0;
        int lastElement = -1;
        boolean flag;

        public boolean hasNext() {
            return nextElement != size();
        }

        public Object next() {
            int i = nextElement;
            if (i >= size()) {
                System.out.println("Элементов нет!");
            }
            nextElement = i + 1;
            lastElement = i;
            flag = true;
            return mas[lastElement];
        }

        public void remove() {
            int offset = 0;
            if (lastElement < 0 || !flag) {
                throw new IllegalStateException();
            }
            MyListImpl.this.remove(mas[lastElement]);
            nextElement = lastElement;
            lastElement = -1;
        }
    }

    public ListIterator listIterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl extends IteratorImpl implements ListIterator {

        ListIteratorImpl() {
            super();
            nextElement = 0;
        }

        boolean flag;

        public boolean hasPrevious() {
            return nextElement != 0;
        }

        public Object previous() {
            int i = nextElement - 1;
            if (i < 0) {
                System.out.println("Элементов нет!");
            }
            nextElement = i;
            lastElement = i;
            flag = true;
            return mas[lastElement];
        }

        public void set(Object e) {
            Object[] mass = new Object[mas.length];
            if (lastElement < 0) {
                System.err.println("Элементов нет!");
            }
            for (int i = 0; i < mass.length; i++) {
                if (i == lastElement) {
                    mass[i] = e;
                } else {
                    mass[i] = mas[i];
                }
            }
            mas = mass;
        }
    }


    public void add(Object e) {
        Object[] mass = new Object[mas.length + 1];
        for (int i = 0; i < mas.length; i++) {
            mass[i] = mas[i];
        }
        mass[mas.length] = e;
        mas = mass;
    }

    public void clear() {
        mas = new Object[0];
    }

    public boolean remove(Object o) {
        Object[] mass = new Object[mas.length - 1];
        int j = 0;
        boolean flag = false;
        if (o == null) {
            for (int i = 0; i < mas.length; i++)
                if (mas[i] == null) {
                    j++;
                    flag = true;
                    continue;
                } else {
                    mass[i - j] = mas[i];
                }
        } else {
            for (int i = 0; i < mas.length; i++)
                if (o.equals(mas[i])) {
                    j++;
                    flag = true;
                    continue;
                } else {
                    mass[i - j] = mas[i];
                }
        }
        mas = mass;
        return flag;
    }

    public Object[] toArray() {
        return mas;
    }

    public int size() {
        return mas.length;
    }

    public boolean contains(Object o) {
        if (o == null) {
            for (int i = 0; i < mas.length; i++)
                if (mas[i] == null)
                    return true;
        } else {
            for (int i = 0; i < mas.length; i++)
                if (o.equals(mas[i]))
                    return true;
        }
        return false;
    }

    public boolean containsAll(MyList c) {
        for (Object i : c.toArray()) {
            if (!contains(i)) return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sStart = new StringBuilder("[");
        StringBuilder sEnd = new StringBuilder("]");
        if (mas.length == 0) {
            return sStart.append(sEnd).toString();
        }
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == null) {
                if (i != mas.length - 1) {
                    sStart = sStart.append("null").append(',');
                } else {
                    sStart = sStart.append("null");
                }
            } else {
                if (i == mas.length - 1) {
                    sStart = sStart.append(String.valueOf(mas[i]));
                } else {
                    sStart = sStart.append(String.valueOf(mas[i])).append(',');
                }
            }
        }
        sStart.append(sEnd);
        return sStart.toString();
    }
}
