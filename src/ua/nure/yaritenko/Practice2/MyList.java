package ua.nure.yaritenko.Practice2;

/**
 * Created by student on 03.11.2017.
 */
public interface MyList extends Iterable<Object> {
    void add(Object e);
    void clear();
    boolean remove(Object o);
    Object[] toArray();
    int size();
    boolean contains(Object o);
    boolean containsAll(MyList c);


}
